from mpi4py import MPI
from sys import argv
import numpy as np

# Problem 2
"""Pass a random NumPy array of shape (n,) from the root process to process 1,
where n is a command-line argument. Print the array and process number from
each process.

Usage:
    # This script must be run with 2 processes.
    $ mpiexec -n 2 python problem2.py 4
    Process 1: Before checking mailbox: vec=[ 0.  0.  0.  0.]
    Process 0: Sent: vec=[ 0.03162613  0.38340242  0.27480538  0.56390755]
    Process 1: Recieved: vec=[ 0.03162613  0.38340242  0.27480538  0.56390755]
"""
from sys import argv
from mpi4py import MPI
import numpy as np

# Get the communication variables
COMM = MPI.COMM_WORLD
RANK = COMM.Get_rank()

# Get the value of n from command line
n = int(argv[1])

# Initialize vector in both
vec = np.zeros(n)


if RANK == 0:
    # First processor: Fill random values
    vec = np.random.rand(n)

    # Send random vector to other process
    COMM.Send(vec,dest=1)
    print("Process {}: Sent: vec={}".format(RANK, vec))


if RANK == 1:
    # Print initial empty vector
    print("Process {}: Before checking mailbox: vec={}".format(RANK, vec))

    # Check the mailbox and report back the results
    COMM.Recv(vec,source = 0)
    print("Process {}: Recieved: vec={}".format(RANK, vec))
