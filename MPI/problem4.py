from mpi4py import MPI
from sys import argv
from scipy import linalg as la
import numpy as np

# Problem 4
"""The n-dimensional open unit ball is the set U_n = {x in R^n : ||x|| < 1}.
Estimate the volume of U_n by making N draws on each available process except
for the root process. Have the root process print the volume estimate.

Command line arguments:
    n (int): the dimension of the unit ball.
    N (int): the number of random draws to make on each process but the root.

Usage:
    # Estimate the volume of U_2 (the unit circle) with 2000 draws per process.
    $ mpiexec -n 4 python problem4.py 2 2000
    Volume of 2-D unit ball: 3.13266666667      # Results will vary slightly.
"""

# Pick n and N from command line arguments
n = int(argv[1])
N = int(argv[2])

# Get each rank
COMM = MPI.COMM_WORLD
SIZE = COMM.Get_size()
RANK = COMM.Get_rank()

if RANK != 0:
    # Draw N n-tuples between -1 and 1
    points = np.random.uniform(-1,1,(N,n))
    norms = np.linalg.norm(points, axis=1, ord=2)
    M = float(sum(norms < 1))

    # Send the number of vectors within U_n (M) to root
    COMM.Send(np.array([M]),dest=0)

else:
    # Wait for all
    total = np.zeros(1)
    temp = np.zeros(1)
    for i in range(1,SIZE):

        COMM.Recv(temp,source=i)
        total = total + temp

    # Calculate pi estimate
    pi = 2**n * total / (N * (SIZE - 1))

    print("estimated volume = {}".format(pi))
