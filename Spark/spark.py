# solutions.py

import pyspark
from pyspark.sql import SparkSession
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
from pyspark.ml import Pipeline
from pyspark.ml.feature import VectorAssembler, StringIndexer, OneHotEncoderEstimator
from pyspark.ml.tuning import ParamGridBuilder, TrainValidationSplit
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.evaluation import MulticlassClassificationEvaluator as MCE

# ---------------------------- Helper Functions ---------------------------- #

def plot_crime(data, category):
    """
    Helper function that plots the crime and income data from problem 4.
    Parameters:
        data ((n, 3) nparray, dtype=str (or '<U22')); format should be:
            First Column:   borough name
            Second Column:  crime rate (i.e. avg crimes per month)
            Third Column:   category (i.e. minor category) of crime
    Returns: None
    """
    domain = np.arange(len(data))
    fig, ax = plt.subplots()

    # plot number of months with
    p1 = ax.plot(domain, data[:, 1].astype(float) , color='red', label='Months w/ 1+ {}'.format(category))

    # create and plot on second axis
    ax2 = ax.twinx()
    p2 = ax2.plot(domain, data[:, 2].astype(float), color='green', label='Median Income')

    # create legend
    plots = p1 + p2
    labels = [line.get_label() for line in plots]
    ax.legend(plots, labels, loc=0)

    plt.title('Months w/ 1+ {} with Median Income'.format(category))

    plt.show()



# --------------------- Resilient Distributed Datasets --------------------- #

### Problem 1
def word_count(filename='data-files/huck_finn.txt'):
    """
    A function that counts the number of occurrences unique occurrences of each
    word. Sorts the words by count in descending order.
    Parameters:
        filename (str): filename or path to a text file
    Returns:
        word_counts (list): list of (word, count) pairs for the 20 most used words
    """
    # start the SparkSession
    spark = SparkSession\
            .builder\
            .appName('app_name')\
            .getOrCreate()

    # load the file as an RDD
    huck_finn = spark.sparkContext.textFile('huck_finn.txt')

    # count the number of occurances of each word
    words = huck_finn.flatMap(lambda row: row.split(' '))\
                .map(lambda x : (x,1))\
                .reduceByKey(lambda x,y: x+y)

    # sort the words by count, in descending order
    most_common = words.sortBy(lambda row: -row[1])\
                .take(20)

    # end the SparkSession
    spark.stop()

    # return a list of the (word,count) pairs for the 20 most used words
    return most_common

### Problem 2
def monte_carlo(n=10**5, parts=6):
    """
    Runs a Monte Carlo simulation to estimate the value of pi.
    Parameters:
        n (int): number of sample points per partition
        parts (int): number of partitions
    Returns:
        pi_est (float): estimated value of pi
    """
    # start the SparkSession
    spark = SparkSession\
            .builder\
            .appName('app_name')\
            .getOrCreate()

    # initialize an RDD with n*parts sample points and parts partitions
    inside_unit_circle = spark.sparkContext.parallelize(np.random.random((n*parts,2)) * 2 - 1, parts)\
                            .map(lambda x : np.linalg.norm(x) < 1)\
                            .sum()

    # end the SparkSession
    spark.stop()

    # return your estimate
    return inside_unit_circle / (n * parts) * 4


# ------------------------------- DataFrames ------------------------------- #

### Problem 3
def titanic_df(filename='titanic.csv'):
    """
    Extracts various statistics from the titanic dataset. The dataset has the
    following structure:
    Survived | Class | Name | Sex | Age | Siblings/Spouses Aboard | Parents/Children Aboard | Fare
    Returns:
        """
    # start the SparkSession
    spark = SparkSession\
            .builder\
            .appName('app_name')\
            .getOrCreate()

    # load the dataset into a DataFrame
    schema = ('survived INT, pclass INT, name STRING, sex STRING, age FLOAT, sibsp INT, parch INT, fare FLOAT')
    titanic = spark.read.csv('titanic.csv', schema = schema)

    # how many males/females onboard
    male_count = titanic.filter(titanic.sex == 'male').count()
    female_count = titanic.filter(titanic.sex == 'female').count()

    # extract the female and male survival rates
    male_survived = titanic.filter(titanic.sex == 'male').filter(titanic.survived == 1).count()
    female_survived = titanic.filter(titanic.sex == 'female').filter(titanic.survived == 1).count()


    # end the SparkSession
    spark.stop()

    # return results
    return (female_count, male_count, female_survived / female_count, male_survived / male_count)

### Problem 4
def crime_and_income(f1='london_crime_by_lsoa.csv',
                     f2='london_income_by_borough.csv', min_cat='Murder'):
    """
    Explores crime by borough and income for the specified min_cat
    Parameters:
        f1 (str): path to csv file containing crime dataset
        f2 (str): path to csv file containing income dataset
        min_cat (str): crime minor category to analyze
    returns:
        list: borough names sorted by percent months with crime, descending
    """
    # start the SparkSession
    spark = SparkSession\
            .builder\
            .appName('app_name')\
            .getOrCreate()

    # load the two files as PySpark DataFrames
    # format the data to match the input of plot_crime()
    lsoa = spark.read.csv(f1, header=True, inferSchema=True).select(["minor_category","value","borough"])
    lsoa = lsoa.filter(lsoa.minor_category == min_cat).drop("minor_category")
    lsoa = lsoa.groupBy('borough').mean('value')

    borough = spark.read.csv(f2, header=True, inferSchema=True).select(["borough","median-08-16"])

    # create a new dataframe containing specified values
    joined = lsoa.join(borough, on='borough')
    data = joined.collect()
    print(data)
    # stop the SparkSession
    spark.stop()

    # plot_crime(data, min_cat)

    return list(data[:, 0])

### Problem 5
def titanic_classifier(filename='titanic.csv'):
    """
    Implements a logistic regression to classify the Titanic dataset.
    Parameters:
        filename (str): path to the dataset
    Returns:
        lr_metrics (list): a list of metrics gauging the performance of the model
            ('accuracy', 'weightedPrecision', 'weightedRecall')
    """
    # start the SparkSession

    # load the data

    # clean up the data

    # vectorize the features

    # split test and train data
    train, test = feature_vector.randomSplit([0.75, 0.25], seed=11)

    # initialize logistic regression object

    # train the model

    # make predictions

    # obtain performance metrics
    metrics = ['accuracy', 'weightedPrecision', 'weightedRecall']

    # stop the SparkSession

    # return metric values in order from above
    return
