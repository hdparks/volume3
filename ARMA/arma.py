# Name
# Date
# Class

from scipy.stats.distributions import norm
from scipy.optimize import fmin
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.tsa.arima_model import ARMA
from pydataset import data as pydata
from statsmodels.tsa.stattools import arma_order_select_ic as order_select
import pandas as pd

def arma_forecast_naive(file='weather.npy',p=2,q=1,n=20):
    """
    Perform ARMA(1,1) on data. Let error terms be drawn from
    a standard normal and let all constants be 1.
    Predict n values and plot original data with predictions.

    Parameters:
        file (str): data file
        p (int): order of autoregressive model
        q (int): order of moving average model
        n (int): number of future predictions
    """
    # Load the dataset
    data = np.load(file)

    # create zt to make stationary
    data = data[1:] - data[:-1]


    # set up the arma model parameters
    phi = .5
    theta = .1

    es = np.random.random(q)
    # predict n steps into the future
    for _ in range(n):
        e = np.random.random()
        zt = phi * data[-p:].sum() + theta * es.sum() + e
        data = np.hstack((data,zt))

        # update the e's
        es = np.roll(es,-1)
        es[-1] = e

    domain = np.arange(len(data))
    plt.plot(domain[:-n], data[:-n],label="Old Data")
    plt.plot(domain[-n:], data[-n:],label="New Data")
    plt.title("ARMA({},{}) Naive Forecast".format(p,q))
    plt.legend()
    plt.show()
    return



def arma_likelihood(file='weather.npy', phis=np.array([0]), thetas=np.array([0]), mu=0., std=1.):
    """
    Transfer the ARMA model into state space.
    Return the log-likelihood of the ARMA model.

    Parameters:
        file (str): data file
        phis (ndarray): coefficients of autoregressive model
        thetas (ndarray): coefficients of moving average model
        mu (float): mean of errorm
        std (float): standard deviation of error

    Return:
        log_likelihood (float)
    """
    # load data
    data = np.load(file)

    # create zt to make stationary
    data = data[1:] - data[:-1]

    # Get Kalman properties from state space rep
    F, Q, H, dim_states, dim_time_series = state_space_rep(phis, thetas, mu, std)

    # Get x_hat, P_hat values from Kalman function
    x_hat, P_hat = kalman(F, Q, H, data - mu)

    # Calculate p for each t in z_t
    p = [norm.pdf(x=data[i], loc= H @ x_hat[i] + mu, scale=np.sqrt(H @ P_hat[i] @ H.T) ) for i in range(len(data))]

    # Calculate log likelihood of the given model
    return np.log(p).sum()

def model_identification(file='weather.npy',p=4,q=4):
    """
    Identify parameters to minimize AIC of ARMA(p,q) model

    Parameters:
        file (str): data file
        p (int): maximum order of autoregressive model
        q (int): maximum order of moving average model

    Returns:
        phis (ndarray (p,)): coefficients for AR(p)
        thetas (ndarray (q,)): coefficients for MA(q)
        mu (float): mean of error
        std (float): std of error
    """
    # Load the data
    data = np.load(file)

    # create zt to make stationary
    data = data[1:] - data[:-1]

    # track best performer
    min_AIC = np.inf
    min_cap_theta = None
    sol_p = None
    sol_q = None

    for p in range(1,p+1):
        for q in  range(1,q+1):

            # Assume p, q, and data already defined
            f = lambda x:  -1 * arma_likelihood(file, phis=x[:p],thetas=x[p:p+q],mu=x[-2],std=x[-1])

            # create an initial point
            x0 = np.zeros(p+q+2)
            x0[-2] = data.mean()
            x0[-1] = data.std()
            sol = fmin(f,x0,maxiter=10000, maxfun=10000)
            k = p + q + 2
            AIC = 2 * k * (1 + (k+1)/(len(data) - k)) + 2 * arma_likelihood(file, phis=sol[:p],thetas=sol[p:p+q],mu=sol[-2],std=sol[-1])

            if AIC < min_AIC:
                min_AIC = AIC
                min_cap_theta = sol
                sol_p = p
                sol_q = q

    return (min_cap_theta[:sol_p],min_cap_theta[sol_p:sol_p+sol_q],min_cap_theta[-2],min_cap_theta[-1])


def arma_forecast(file='weather.npy', phis=np.array([0]), thetas=np.array([0]), mu=0., std=1., n=30):
    """
    Forecast future observations of data.

    Parameters:
        file (str): data file
        phis (ndarray (p,)): coefficients of AR(p)
        thetas (ndarray (q,)): coefficients of MA(q)
        mu (float): mean of ARMA model
        std (float): standard deviation of ARMA model
        n (int): number of forecast observations

    Returns:
        new_mus (ndarray (n,)): future means
        new_covs (ndarray (n,)): future standard deviations
    """
    # Load the data
    data = np.load(file)

    # create zt to make stationary
    data = data[1:] - data[:-1]

    # Get Kalman stuff
    F, Q, H, dim_states, dim_time_series = state_space_rep(phis, thetas, mu, std)

    # Use Kalman stuff to predict for n steps
    x_hat, P_hat = kalman(F, Q, H, data - mu)

    # Calculate update for x_hat and P_hat
    x = x_hat[-1]
    P = P_hat[-1]
    y = data[-1] - mu - H @ x
    S = H @ P @ H.T
    K = P @ H.T @ np.linalg.inv(S)
    x_kk = x + K @ y
    P_kk = (np.eye(len(P)) - K @ H) @ P
    pred_x = [F @ x_kk]
    pred_P = [F @ P_kk @ F + Q]

    for _ in range(n-1):
        pred_x.append(F @ pred_x[-1])
        pred_P.append(F @ pred_P[-1] @ F.T + Q)

    # Convert to numpy arrays
    pred_x = np.array(pred_x)
    pred_P = np.array(pred_P)

    # Caluclate predicted zs
    pred_z = (H @ pred_x.T).reshape(n,-1)
    pred_cov = (H @ pred_P @ H.T).reshape(n,-1)
    pred_std = np.sqrt(pred_cov)

    # Caluclate confidence interval
    upper = np.array(pred_z + 2 * pred_std)
    lower = np.array(pred_z - 2 * pred_std)
    # Plot
    domain = np.arange(len(data) + len(pred_z))
    plt.plot(domain[:-n], data,label="Original Data")
    plt.plot(domain[-n:], pred_z, label="Forecast")
    plt.plot(domain[-n:], upper, 'g', label="95% Confidence Interval")
    plt.plot(domain[-n:], lower, 'g')
    plt.legend()
    plt.show()

    # return means and covariances
    return pred_z, pred_cov




def sm_arma(file = 'weather.npy', p=3, q=3, n=30):
    """
    Build an ARMA model with statsmodel and
    predict future n values.

    Parameters:
        file (str): data file
        p (int): maximum order of autoregressive model
        q (int): maximum order of moving average model
        n (int): number of values to predict

    Return:
        aic (float): aic of optimal model
    """
    # Load dataset
    data = np.load(file)

    # create zt to make stationary
    data = data[1:] - data[:-1]

    # Find the optimal model
    min_AIC = np.inf
    optimal_p = None
    optimal_q = None
    for p_ in range(1,p+1):
        for q_ in range(1,q+1):
            model = ARMA(data, order=(p_,q_))
            fit = model.fit(method='mle', trend='c')
            aic = fit.aic
            if aic < min_AIC:
                optimal_p = p_
                optimal_q = q_
                min_AIC = aic

    model = ARMA(endog=data, order=(optimal_p, optimal_q))
    fit = model.fit(method='mle',trend='c')
    predictions = model.predict(params = fit.params, start=0,end=len(data)+30)
    print('p:',optimal_p, 'q:',optimal_q)

    # Plot
    plt.plot(data,label="Old Data")
    plt.plot(predictions, label="ARMA Model")
    plt.legend()
    plt.title("Statsmodel ARMA ({},{})".format(optimal_p,optimal_q))
    plt.xlabel("Hours")
    plt.show()

    return min_AIC


def manaus(start='1983-01-31',end='1995-01-31',p=4,q=4):
    """
    Plot the ARMA(p,q) model of the River Negro height
    data using statsmodels built-in ARMA class.

    Parameters:
        start (str): the data at which to begin forecasting
        end (str): the date at which to stop forecasting
        p (int): max_ar parameter
        q (int): max_ma parameter
    Return:
        aic_min_order (tuple): optimal order based on AIC
        bic_min_order (tuple): optimal order based on BIC
    """
    # Get dataset
    raw = pydata('manaus')
    # Make DateTimeIndex
    manaus = pd.DataFrame(raw.values,index=pd.date_range('1903-01','1993-01',freq='M'))
    manaus = manaus.drop(0,axis=1)
    # Reset column names
    manaus.columns = ['Water Level']
    manaus = manaus[start:end]

    data = np.array(manaus)

    order =  order_select(manaus.values,max_ar = p, max_ma = q, ic=['aic','bic'],fit_kw={'method':'mle'})

    aic_model = ARMA(endog=data, order = order.aic_min_order )
    aic_fit = aic_model.fit(method='mle',trend='c')
    aic_predictions = aic_model.predict(params = aic_fit.params, start=0,end=len(data)+30)

    bic_model = ARMA(endog=data, order = order.bic_min_order )
    bic_fit = bic_model.fit(method="mle",trend='c')
    bic_predictions = bic_model.predict(params = bic_fit.params, start=0,end=len(data)+30)


    domain = np.arange(len(aic_predictions))
    fig,(ax1,ax2) = plt.subplots(2,1)
    plt.suptitle("Statsmodel ARMA: Water Levels in the Rio Negro")
    ax1.plot(data,label="Water Level")
    ax1.plot(aic_predictions,label="AIC Forecast {}".format(order.aic_min_order))
    ax1.fill_between(   domain[len(data):],
                        aic_predictions[len(data):] + 2 * aic_fit.params[-1],
                        aic_predictions[len(data):] - 2 * aic_fit.params[-1], label="(95% Confidence Interval)" )
    ax1.legend()

    ax2.plot(data,label="Water Level")
    ax2.plot(bic_predictions, label="BIC Forecast {}".format(order.bic_min_order))
    ax2.fill_between(   domain[len(data):],
                        aic_predictions[len(data):] + 2 * bic_fit.params[-1],
                        aic_predictions[len(data):] - 2 * bic_fit.params[-1],label="(95% Confidence Interval)" )
    ax2.legend()
    plt.show()
    plt.ylabel("Date")
    plt.xlabel("Number of Sunspots")
    return order['aic_min_order'], order['bic_min_order']
###############################################################################

def kalman(F, Q, H, time_series):
    # Get dimensions
    dim_states = F.shape[0]

    # Initialize variables
    # covs[i] = P_{i | i-1}
    covs = np.zeros((len(time_series), dim_states, dim_states))
    mus = np.zeros((len(time_series), dim_states))

    # Solve of for first mu and cov
    covs[0] = np.linalg.solve(np.eye(dim_states**2) - np.kron(F,F),np.eye(dim_states**2)).dot(Q.flatten()).reshape(
            (dim_states,dim_states))
    mus[0] = np.zeros((dim_states,))

    # Update Kalman Filter
    for i in range(1, len(time_series)):
        t1 = np.linalg.solve(H.dot(covs[i-1]).dot(H.T),np.eye(H.shape[0]))
        t2 = covs[i-1].dot(H.T.dot(t1.dot(H.dot(covs[i-1]))))
        covs[i] = F.dot((covs[i-1] - t2).dot(F.T)) + Q
        mus[i] = F.dot(mus[i-1]) + F.dot(covs[i-1].dot(H.T.dot(t1))).dot(
                time_series[i-1] - H.dot(mus[i-1]))
    return mus, covs

def state_space_rep(phis, thetas, mu, sigma):
    # Initialize variables
    dim_states = max(len(phis), len(thetas)+1)
    dim_time_series = 1 #hardcoded for 1d time_series

    F = np.zeros((dim_states,dim_states))
    Q = np.zeros((dim_states, dim_states))
    H = np.zeros((dim_time_series, dim_states))

    # Create F
    F[0][:len(phis)] = phis
    F[1:,:-1] = np.eye(dim_states - 1)
    # Create Q
    Q[0][0] = sigma**2
    # Create H
    H[0][0] = 1.
    H[0][1:len(thetas)+1] = thetas

    return F, Q, H, dim_states, dim_time_series
