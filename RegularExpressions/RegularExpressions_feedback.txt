09/05/19 15:06

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (5 points):
Score += 5

Problem 4 (5 points):
Score += 5

Problem 5 (5 points):
prob5() failed
	Correct response: 'k, i, p = 999, 1, 0\n        while k > i:\n            i *= 2\n            p += 1\n            if k != 999:\n                print("k should not have changed")\n            else:\n                pass\n        print(p)'
	Student response: 'k, i, p = 999, 1, 0\n        while k > i\n            i *= 2\n            p += 1\n            if k != 999\n                print("k should not have changed")\n            else\n                pass\n        print(p)'
prob5() failed
	Correct response: 'for _ in range(5):\n            if x > 10:\n                try:\n                    x /= 2\n                except ValueError as e:\n                    print(e)\n                except:\n                    print(x)\n                finally:\n                    x += 1\n            elif x == 5:\n                x = 6\n            else:\n                while x < 4:\n                    x += 2\n        def func():\n            pass\n        class something:\n            pass'
	Student response: 'for _ in range(5)\n            if x > 10\n                try\n                    x /= 2\n                except ValueError as e\n                    print(e)\n                except\n                    print(x)\n                finally\n                    x += 1\n            elif x == 5\n                x = 6\n            else\n                while x < 4\n                    x += 2\n        def func()\n            pass\n        class something\n            pass'
Score += 0

Problem 6 (20 points):
AttributeError: module 're' has no attribute 'complie'

Code Quality (5 points):
Score += 5

Total score: 25/50 = 50.0%

-------------------------------------------------------------------------------

09/11/19 13:42

Problem 1 (5 points):
Score += 5

Problem 2 (5 points):
Score += 5

Problem 3 (5 points):
Score += 5

Problem 4 (5 points):
Score += 5

Problem 5 (5 points):
prob5() failed
	Correct response: 'for _ in range(5):\n            if x > 10:\n                try:\n                    x /= 2\n                except ValueError as e:\n                    print(e)\n                except:\n                    print(x)\n                finally:\n                    x += 1\n            elif x == 5:\n                x = 6\n            else:\n                while x < 4:\n                    x += 2\n        def func():\n            pass\n        class something:\n            pass'
	Student response: 'for _ in range(5):\n            if x > 10:\n                try:\n                    x /= 2\n                except ValueError as e:\n                    print(e)\n                except:\n                    print(x)\n                finally:\n                    x += 1\n            elif x == 5:\n                x = 6\n            else:\n                while x < 4:\n                    x += 2\n        def func()\n            pass\n        class something\n            pass'
Score += 2

Problem 6 (20 points):
Score += 20

Code Quality (5 points):
Score += 5

Total score: 47/50 = 94.0%

Great job!


Comments:
	good work

-------------------------------------------------------------------------------

