# regular_expressions.py
"""Volume 3: Regular Expressions.
<Name>
<Class>
<Date>
"""

import re

# Problem 1
def prob1():
    """Compile and return a regular expression pattern object with the
    pattern string "python".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """
    #   Good to be back!

    pattern = re.compile('python')

    return pattern

# Problem 2
def prob2():
    """Compile and return a regular expression pattern object that matches
    the string "^{@}(?)[%]{.}(*)[_]{&}$".

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """

    pattern = re.compile(r"\^\{\@\}\(\?\)\[\%\]\{\.\}\(\*\)\[\_\]\{\&\}\$")

    return pattern

# Problem 3
def prob3():
    """Compile and return a regular expression pattern object that matches
    the following strings (and no other strings).

        Book store          Mattress store          Grocery store
        Book supplier       Mattress supplier       Grocery supplier

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """

    pattern = re.compile("^(Book|Mattress|Grocery) (store|supplier)$")

    return pattern

# Problem 4
def prob4():
    """Compile and return a regular expression pattern object that matches
    any valid Python identifier.

    Returns:
        (_sre.SRE_Pattern): a compiled regular expression pattern object.
    """

    pattern = re.compile(r"^[A-Za-z_]\w* *(= *([A-Za-z_]\w*|\'[^\']*\'|-?[0-9]+(\.[0-9]*)?))?$")

    # Test cases
    # test_cases = ["max=4.2", "string= ''", "num_guesses", "a =  '    '", "i = "]
    # for i in test_cases:
    #     print(bool(pattern.match(i)))

    # Everything is working as it should!

    return pattern


# Problem 5
def prob5(code):
    """Use regular expressions to place colons in the appropriate spots of the
    input string, representing Python code. You may assume that every possible
    colon is missing in the input string.

    Parameters:
        code (str): a string of Python code without any colons.

    Returns:
        (str): code, but with the colons inserted in the right places.
    """
    # In the current state, I'm placing a colon at the end of the line,
    # without parsing the expression -- basically, any non-return character is
    # considered a vaild expression.

    pattern = re.compile(r"(else|finally|try|(if|elif|for|while|except).*)$", re.MULTILINE)

    # Add a colon to the end of each that needs it
    code = pattern.sub(r"\1:", code)
    return code


# Problem 6
def prob6(filename="fake_contacts.txt"):
    """Use regular expressions to parse the data in the given file and format
    it uniformly, writing birthdays as mm/dd/yyyy and phone numbers as
    (xxx)xxx-xxxx. Construct a dictionary where the key is the name of an
    individual and the value is another dictionary containing their
    information. Each of these inner dictionaries should have the keys
    "birthday", "email", and "phone". In the case of missing data, map the key
    to None.

    Returns:
        (dict): a dictionary mapping names to a dictionary of personal info.
    """

    # Read in the data as a list of strings
    with open(filename) as file:
        data = file.readlines()

    # Create the dictionary
    contacts = dict()

    # Compile the patterns
    name_pattern = re.compile(r"(\w+( \w\.)? \w+)")
    email_pattern = re.compile(r"[\w\.]+@[\w\.]+")
    birthday_pattern = re.compile(r"(\d{1,2})\/(\d{1,2})\/(\d{2,4})")
    phone_pattern = re.compile(r"1?\(?(\d{3})\)?-?(\d{3})-?(\d{4})")

    # # Search each entry for each pattern
    for entry in data:
        # Establish name
        name = name_pattern.findall(entry)[0][0]
        contacts[name] = dict()

        # Establish birthday
        birthday = birthday_pattern.findall(entry)
        if len(birthday) == 0:
            contacts[name]["birthday"] = None
        else:
            # reformat birthday
            birthday = list(birthday[0])
            if len(birthday[0]) == 1:
                print(birthday)
                birthday[0] = "0"+birthday[0]
            if len(birthday[1]) == 1:
                birthday[1] = "0"+birthday[1]
            if len(birthday[2]) == 2:
                birthday[2] = "20"+birthday[2]
            contacts[name]["birthday"] = birthday[0]+"/"+birthday[1]+"/"+birthday[2]


        # Establish email
        email = email_pattern.findall(entry)
        if len(email) == 0:
            contacts[name]["email"] = None
        else:
            contacts[name]["email"] = email[0]


        # Establish phone
        phone = phone_pattern.findall(entry)
        if len(phone) == 0:
            contacts[name]["phone"] = None
        else:
            phone = phone[0]
            contacts[name]["phone"] = "("+phone[0]+")"+phone[1]+"-"+phone[2]

    return contacts
