"""Volume 3: Web Scraping.
<Name>
<Class>
<Date>
"""
import requests
import os
from bs4 import BeautifulSoup
import re
from matplotlib import pyplot as plt
import numpy as np


# Problem 1
def prob1():
    """Use the \li{requests} library to get the HTML source for the website
    http://www.example.com.
    Save the source as a file called example.html.
    If the file already exists, do not scrape the website or overwrite the file.
    """
    # Don't scrape again! Reduce, Reuse, Recycle!
    if os.path.exists("example.html"):
        return

    # Scrape
    res = requests.get("http://example.com")

    # Store
    with open("example.html","w") as outfile:
        outfile.write(res.text)


# Problem 2
def prob2():
    """Examine the source code of http://www.example.com. Determine the names
    of the tags in the code and the value of the 'type' attribute associated
    with the 'style' tag.

    Returns:
        (set): A set of strings, each of which is the name of a tag.
        (str): The value of the 'type' attribute in the 'style' tag.
    """
    tag_names = set("html","head","title","meta","style","body","div","h1","p","a")
    typeattr = "text/css"

    return tag_names, typeattr

# Problem 3
def prob3(code):
    """Return a list of the names of the tags in the given HTML code."""
    # Soupify the given html code
    soup = BeautifulSoup(code, 'html.parser')

    # get the name of each tag in the soup
    return [tag.name for tag in soup.find_all()]

# Problem 4
def prob4(filename="example.html"):
    """Read the specified file and load it into BeautifulSoup. Find the only
    <a> tag with a hyperlink and return its text.
    """
    # Read in the data from the given file
    with open(filename, 'r') as myfile:
        html = myfile.read()

    # Soupify
    soup = BeautifulSoup(html, 'html.parser')

    # Get "a" tags
    a_tags = soup.find_all("a")

    # Look for the first "a" tag with href attribute, and return its text
    for a in a_tags:
        if a.attrs.get("href") != None:
            return a.text

# Problem 5
def prob5(filename="san_diego_weather.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the following tags:

    1. The tag containing the date 'Thursday, January 1, 2015'.
    2. The tags which contain the links 'Previous Day' and 'Next Day'.
    3. The tag which contains the number associated with the Actual Max
        Temperature.

    Returns:
        (list) A list of bs4.element.Tag objects (NOT text).
    """
    # Read in html
    with open(filename,'r') as myfile:
        html = myfile.read()

    # Soupify
    soup = BeautifulSoup(html,'html.parser')

    # find the tag containing "Thursday, January 1, 2015"
    t1 = soup.find("h2", attrs={"class":"history-date"})

    # find the tag containins the links "Previous Day" and "Next Day"
    t2prev = soup.find("a", text="« Previous Day")
    t2next = soup.find("a",text="Next Day »")

    # find the tag which contains the number Actual Max Temperature
    t3 = soup.find_all("span",attrs={"class":"wx-value"})[2]

    return [t1, t2prev, t2next, t3]


# Problem 6
def prob6(filename="large_banks_index.html"):
    """Read the specified file and load it into BeautifulSoup. Return a list
    of the tags containing the links to bank data from September 30, 2003 to
    December 31, 2014, where the dates are in reverse chronological order.

    Returns:
        (list): A list of bs4.element.Tag objects (NOT text).
    """
    # read in html
    with open(filename,'r') as myfile:
        html = myfile.read()

    # Soupify
    soup = BeautifulSoup(html,'html.parser')

    # Return all but the first one, since the first one is more recent than September 30th
    return soup.find_all("a",text=re.compile(r"(September|March|June|December)"))[1:]


# Problem 7
def prob(filename="large_banks_data.html"):
    """Read the specified file and load it into BeautifulSoup. Create a single
    figure with two subplots:

    1. A sorted bar chart of the seven banks with the most domestic branches.
    2. A sorted bar chart of the seven banks with the most foreign branches.

    In the case of a tie, sort the banks alphabetically by name.
    """

    # read in HTML
    with open(filename,'r') as myfile:
        html = myfile.read()

    # Soupify
    soup = BeautifulSoup(html,'html.parser')

    # get the right table
    table = soup.find("table", attrs={'rules':"GROUPS"})

    # get a tuple of name, domestic branches, foreign branches
    table = table.tbody

    parseInt = lambda x: int(x.replace(',',''))

    dom_banks = []
    for_banks = []

    for row in table.find_all('tr'):
        items = row.find_all('td')
        name = items[0]
        domestic = items[-4]
        foreign = items[-3]

        if domestic.text != '.':
            dom_banks.append([name,parseInt(domestic.text)])

        if foreign.text != '.':
            for_banks.append([name,parseInt(foreign.text)])


    # sort banks for domestic column
    dom_banks.sort(key=lambda x:x[1])
    dom_banks = np.array(dom_banks)

    # sort banks by foreign branches
    for_banks.sort(key=lambda x:x[1])
    for_banks = np.array(for_banks)

    # create a single figure with 2 subplots
    domain = np.arange(7)
    ax1, ax2 = plt.subplot(211), plt.subplot(212)

    ax1.barh(domain, dom_banks[-7:,1],tick_label=dom_banks[-7:,0])
    ax1.set_title("Domestic branches")
    ax1.set_xlabel("# of Branches")

    ax2.barh(domain, for_banks[-7:,1], tick_label=for_banks[-7:,0])
    ax2.set_title("Foreign branches")
    ax2.set_xlabel("# of Branches")

    plt.tight_layout()
    plt.show()
