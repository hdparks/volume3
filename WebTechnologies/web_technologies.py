# solutions.py
"""Volume 3: Web Technologies. Solutions File."""

import json
import socket
from collections import Counter
from matplotlib import pyplot as plt
import numpy as np


# Problem 1
def prob1(filename="nyc_traffic.json"):
    """Load the data from the specified JSON file. Look at the first few
    entries of the dataset and decide how to gather information about the
    cause(s) of each accident. Make a readable, sorted bar chart showing the
    total number of times that each of the 7 most common reasons for accidents
    are listed in the data set.
    """
    # read the json file to a python object
    with open(filename, 'r') as infile:
        traffic_data = json.load(infile)

    # make a list of the causes
    causes = []
    for incident in traffic_data:
        for i in range(1,7):
            c = incident.get(str("contributing_factor_vehicle_"+ str(i)))
            if c: causes.append(c)

    # Use Counter to quickly sum the causes
    counts = Counter(causes)

    # Pull out the most common 7
    key_causes = counts.most_common(7)

    # Plot the bar graph
    x = np.arange(7)
    plt.barh(x,[i[1] for i in key_causes])
    plt.yticks(x, [i[0] for i in key_causes], ha='right')
    plt.title("7 Most Common Causes of Accidents")
    plt.xlabel("Number of Incidents Reported")
    plt.tight_layout()
    plt.show()

    return



class TicTacToe:
    def __init__(self):
        """Initialize an empty board. The O's go first."""
        self.board = [[' ']*3 for _ in range(3)]
        self.turn, self.winner = "O", None

    def move(self, i, j):
        """Mark an O or X in the (i,j)th box and check for a winner."""
        if self.winner is not None:
            raise ValueError("the game is over!")
        elif self.board[i][j] != ' ':
            raise ValueError("space ({},{}) already taken".format(i,j))
        self.board[i][j] = self.turn

        # Determine if the game is over.
        b = self.board
        if any(sum(s == self.turn for s in r)==3 for r in b):
            self.winner = self.turn     # 3 in a row.
        elif any(sum(r[i] == self.turn for r in b)==3 for i in range(3)):
            self.winner = self.turn     # 3 in a column.
        elif b[0][0] == b[1][1] == b[2][2] == self.turn:
            self.winner = self.turn     # 3 in a diagonal.
        elif b[0][2] == b[1][1] == b[2][0] == self.turn:
            self.winner = self.turn     # 3 in a diagonal.
        else:
            self.turn = "O" if self.turn == "X" else "X"

    def empty_spaces(self):
        """Return the list of coordinates for the empty boxes."""
        return [(i,j) for i in range(3) for j in range(3)
                                        if self.board[i][j] == ' ' ]
    def __str__(self):
        return "\n---------\n".join(" | ".join(r) for r in self.board)


# Problem 2
class TicTacToeEncoder(json.JSONEncoder):
    """A custom JSON Encoder for TicTacToe objects."""
    def default(self, obj):
        if not isinstance(obj, TicTacToe):
            raise TypeError("expected a TicTacToe object")

        return {
            "turn":obj.turn,
            "winner":obj.winner,
            "board":obj.board }


# Problem 2
def tic_tac_toe_decoder(obj):
    """A custom JSON decoder for TicTacToe objects."""
    t = TicTacToe()
    t.turn = obj.get("turn")
    t.winner = obj.get("winner")
    t.board = obj.get("board")
    return t


def mirror_server(server_address=("0.0.0.0", 33333)):
    """A server for reflecting strings back to clients in reverse order."""
    print("Starting mirror server on {}".format(server_address))

    # Specify the socket type, which determines how clients will connect.
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind(server_address)    # Assign this socket to an address.
    server_sock.listen(1)               # Start listening for clients.

    while True:
        # Wait for a client to connect to the server.
        print("\nWaiting for a connection...")
        connection, client_address = server_sock.accept()

        try:
            # Receive data from the client.
            print("Connection accepted from {}.".format(client_address))
            in_data = connection.recv(1024).decode()    # Receive data.
            print("Received '{}' from client".format(in_data))

            # Process the received data and send something back to the client.
            out_data = in_data[::-1]
            print("Sending '{}' back to the client".format(out_data))
            connection.sendall(out_data.encode())       # Send data.

        # Make sure the connection is closed securely.
        finally:
            connection.close()
            print("Closing connection from {}".format(client_address))

def mirror_client(server_address=("0.0.0.0", 33333)):
    """A client program for mirror_server()."""
    print("Attempting to connect to server at {}...".format(server_address))

    # Set up the socket to be the same type as the server.
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect(server_address)    # Attempt to connect to the server.
    print("Connected!")

    # Send some data from the client user to the server.
    out_data = input("Type a message to send: ")
    client_sock.sendall(out_data.encode())              # Send data.

    # Wait to receive a response back from the server.
    in_data = client_sock.recv(1024).decode()           # Receive data.
    print("Received '{}' from the server".format(in_data))

    # Close the client socket.
    client_sock.close()


# Problem 3
def tic_tac_toe_server(server_address=("0.0.0.0", 44444)):
    """A server for playing tic-tac-toe with random moves."""


    print("Starting TicTacToe server on {}".format(server_address))

    # Specify the socket type, which determines how clients will connect.
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.bind(server_address)    # Assign this socket to an address.
    server_sock.listen(1)               # Start listening for clients.

    while True:

        # Wait for a client to connect to the server.
        print("\nWaiting for a connection...")
        connection, client_address = server_sock.accept()
        print("Connection accepted from {}.".format(client_address))

        def send_data(data):
            print("Sending '{}' back to the client".format(data))
            connection.sendall(data.encode())

        try:
            while True:
                # Receive data from the client.
                in_data = connection.recv(1024).decode()    # Receive data.
                print("Received '{}' from client".format(in_data))

                # Process the data
                game = json.loads(in_data, object_hook=tic_tac_toe_decoder)

                if not game.winner == None:
                     # client just won!
                     game.winner = "WIN"
                     send_data(json.dumps(game, cls=TicTacToeEncoder))
                     break

                # get open spots
                open_spots = game.empty_spaces()

                # If board is full,
                if not len(open_spots):
                    # it's a DRAW
                    game.winner = "DRAW"
                    send_data(json.dumps(game, cls=TicTacToeEncoder))
                    break

                # make a random move
                move = open_spots[np.random.choice(len(open_spots))]
                print("making move: ", move)
                game.move(*move)

                if not game.winner == None:
                    game.winner = "LOSE"
                    send_data(json.dumps(game, cls=TicTacToeEncoder))
                    break

                else:
                    send_data(json.dumps(game, cls=TicTacToeEncoder))

        # Make sure the connection is closed securely.
        finally:
            connection.close()
            print("Closing connection from {}".format(client_address))







# Problem 4
def tic_tac_toe_client(server_address=("0.0.0.0", 44444)):
    """A client program for tic_tac_toe_server()."""
    print("Attempting to connect to server at {}...".format(server_address))

    # Set up the socket to be the same type as the server.
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect(server_address)    # Attempt to connect to the server.
    print("Connected!")

    # Initialize a new TicTacToe object
    game = TicTacToe()

    while True:
        # Print the board
        print(game)

        # Prompt the player for a move
        valid_moves = game.empty_spaces()
        move = input("Type the coordinates of your next move: ")
        move = (int(move[0]),int(move[2]))
        while move not in valid_moves:
            print("Invalid coordinate, try again", move)
            move = input()
            move = (int(move[0]),int(move[2]))

        # Do move
        game.move(*move)

        # Serialize the game, and send it to the server
        out_data = json.dumps(game, cls=TicTacToeEncoder)

        client_sock.sendall(out_data.encode())              # Send data.

        # Wait to receive a response back from the server.
        in_data = client_sock.recv(1024).decode()           # Receive data.
        print("Received '{}' from the server".format(in_data))

        game = json.loads(in_data, object_hook = tic_tac_toe_decoder)

        # Check for end conditions
        if game.winner == "WIN":
            print("Congratulations, you win!")
            print("Final board:")
            print(game)
            break

        elif game.winner == 'DRAW':
            print("The game is a draw.")
            print("Final board:")
            print(game)
            break

        elif game.winner == 'LOSE':
            print("Sorry, you lose!")
            print("Final board:")
            print(game)
            break

        else:
            game = json.loads(in_data, object_hook = tic_tac_toe_decoder)
    # Close the client socket.
    client_sock.close()
